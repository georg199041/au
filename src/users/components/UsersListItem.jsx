import React from 'react'
import moment from 'moment'

export default (props) => (
  <li>
    <div>{props.user.name}</div>
    <div>{props.user.email}</div>
    <div>{moment(props.user.registerDate).format("MMM Do YY")}</div>
    <div>{moment(props.user.registerDate, "YYYYMMDD").fromNow()}</div>
  </li>
)