import React from 'react'
import UsersList from './users/components/UsersListContainer'
import './App.css'

const App = () => (
  <div>
    <UsersList />
  </div>
)

export default App