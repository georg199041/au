import * as types from './constants'

export const getUsersAction = (payload) => ({ type: types.UPDATE_USERS, payload })

export const searchUsersAction = (payload) => ({ type: types.SEARCH_USERS, payload })
