/**
 * Mocking client-server processing
 */
import _users from './users.json'

const TIMEOUT = 100

export default {
  getUsers: () => new Promise((resolve) => {
    setTimeout(() => resolve(_users), TIMEOUT)
  })
}