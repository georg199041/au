import * as types from './constants'

const initialState = []

let usersCache = []

export default (state = initialState, action) => {
  switch (action.type) {
    case types.UPDATE_USERS:
      state = action.payload
      usersCache = state
      return state
    case types.SEARCH_USERS:
      // cache previously queried users to restore them in case of empty query
      state = action.payload.length ? usersCache.filter(item => (
        item.name.toLowerCase().includes(action.payload.toLowerCase()) ||
        item.email.toLowerCase().includes(action.payload.toLowerCase()))) : [ ...usersCache ]
      return state
    default:
      return state
  }
}
