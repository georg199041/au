import React from 'react'
import PropTypes from 'prop-types'
import api from '../../api/api'
import UsersListItem from './UsersListItem'
import UsersSearchField from './UsersSearchField'

class UsersList extends React.Component {
  componentDidMount() {
    api.getUsers()
      .then((data) => this.props.getUsersAction(data))
  }

  render() {
    return (
      <div>
        <UsersSearchField searchUsers={this.props.searchUsers} />
        <ul>
          {this.props.users.map(item => <UsersListItem key={item.id} user={item} />)}
        </ul>

      </div>
    )
  }
}

UsersList.propTypes = {
  users: PropTypes.array.isRequired
}

export default UsersList