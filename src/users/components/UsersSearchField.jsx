import React from 'react'
import PropTypes from 'prop-types'

class UsersSearchField extends React.Component {
  render() {
    return (
      <div>
        <form>
          <i class="material-icons">account_box</i>
          <input type="text" placeholder="search" onKeyUp={(e) => this.props.searchUsers(e.target.value)} />
        </form>
      </div>
    )
  }
}

UsersSearchField.propTypes = {
  searchUsers: PropTypes.func.isRequired
}

export default UsersSearchField