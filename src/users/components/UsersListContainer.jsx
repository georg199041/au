import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import UsersList from './UsersList'
import { getUsersAction, searchUsersAction } from '../actions'

const mapStateToProps = (state) => ({
  users: state.users
})

const mapDispatchToProps = (dispatch) => ({
  getUsersAction: bindActionCreators(getUsersAction, dispatch),
  searchUsers: bindActionCreators(searchUsersAction, dispatch)
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList)
